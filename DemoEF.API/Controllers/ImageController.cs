﻿using DemoEF.API.Services;
using EFDemo.Entities;
using EFDemo.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace DemoEF.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private readonly ImageService _service;

        public ImageController(ImageService service)
        {
            _service = service;
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            Image image = _service.FindById(id);
            if (image is null) return NotFound();

            //string mimetype = null;

            //switch (image.MimeType)
            //{
            //    case ImageTypes.ImageJpg:
            //        mimetype = "image/jpg";
            //        break;
            //    case ImageTypes.ImagePng:
            //        mimetype = "image/png";
            //        break;
            //    case ImageTypes.ImageSvg:
            //        mimetype = "image/svg";
            //        break;
            //}

            return File(image.File, ToMemberString(image.MimeType));
        }

        private string ToMemberString<T>(T enumeration) {
            EnumMemberAttribute a = enumeration.GetType()
                .GetMember(enumeration.ToString())[0]
                .GetCustomAttributes(typeof(EnumMemberAttribute), false)
                .Cast<EnumMemberAttribute>().FirstOrDefault();
            return a.Value;
        }
    }
}
