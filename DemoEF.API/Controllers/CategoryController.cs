﻿using DemoEF.API.Dto;
using DemoEF.API.Dto.Forms;
using DemoEF.API.Security;
using DemoEF.API.Services;
using EFDemo;
using EFDemo.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoEF.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly CategoryService _service;

        public CategoryController(CategoryService service)
        {
            _service = service;
        }

        [HttpGet]
        [Produces("application/json", Type = typeof(IEnumerable<CategoryDto>))]
        public IActionResult Get()
        {
            return Ok(_service.FindAll());
        }

        [HttpGet("{id}")]
        [Produces("application/json", Type = typeof(CategoryDetailsDto))]
        public IActionResult Get(int id)
        {
            CategoryDetailsDto dto = _service.FindById(id);
            if (dto == null)
                return NotFound();
            return Ok(dto);
        }

        [HttpPost]
        [Produces("application/json", Type = null)]
        [ApiAuthorize("ADMIN", "CUSTOMER")]
        public IActionResult Post([FromBody]CategoryFormDto form)
        {
            _service.Add(form);
            return NoContent();
        }

        [HttpPut]
        [Produces("application/json", Type = null)]
        [ApiAuthorize("ADMIN")]
        public IActionResult Put([FromBody]CategoryFormDto form)
        {
            _service.Update(form);
            return NoContent();
        }

        [HttpDelete("{id}")]
        [ApiAuthorize("ADMIN")]
        [Produces("application/json", Type = null)]
        public IActionResult Delete(int id)
        {
            _service.Delete(id);
            return NoContent();
        }
    }
}
