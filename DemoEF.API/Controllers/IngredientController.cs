﻿using DemoEF.API.Dto;
using DemoEF.API.Dto.Forms;
using DemoEF.API.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoEF.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IngredientController : ControllerBase
    {
        private readonly IngredientService _service;

        public IngredientController(IngredientService service)
        {
            _service = service;
        }

        [HttpGet]
        public IActionResult Get([FromQuery]int cocktailId)
        {
            return Ok(_service.FindByCocktail(cocktailId));
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            IngredientDto dto = _service.FindById(id);
            if (dto is null) return NotFound();
            return Ok(dto);
        }

        [HttpPost]
        public IActionResult Post(IngredientFormDto dto)
        {
            try
            {
                _service.Add(dto);
                return NoContent();
            }catch(Exception e)
            {
                return BadRequest();
            }
        }

        [HttpPut]
        public IActionResult Put(IngredientFormDto dto)
        {
            try
            {
                _service.Update(dto);
                return NoContent();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            try
            {
                _service.Delete(id);
                return NoContent();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }
    }
}
