﻿using DemoEF.API.Dto;
using DemoEF.API.Dto.Forms;
using DemoEF.API.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoEF.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CocktailController : ControllerBase
    {

        private readonly CocktailService _service;

        public CocktailController(CocktailService service)
        {
            _service = service;
        }

        [HttpGet]
        public IActionResult Get([FromQuery]int limit, [FromQuery]int offset)
        {
            PaginationDto<CocktailDto> dto = _service.Find(limit, offset);
            return Ok(dto);
        }


        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            CocktailDetailsDto dto = _service.FindById(id);
            if (dto is null) return NotFound();
            return Ok(dto);
        }

        [HttpPost]
        public IActionResult Post(CocktailFormDto dto)
        {
            try
            {
                _service.Add(dto);
                return NoContent();

            }catch(Exception e)
            {
                return BadRequest();
            }
        }
    }
}
