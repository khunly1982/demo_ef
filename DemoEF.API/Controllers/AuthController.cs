﻿using Cognitic.Tools.Security.Jwt.Services;
using DemoEF.API.Dto;
using DemoEF.API.Dto.Forms;
using DemoEF.API.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoEF.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly AuthService _service;

        private readonly JwtService _tokenService;

        public AuthController(AuthService service, JwtService jwtService)
        {
            _service = service;
            _tokenService = jwtService;
        }

        [HttpPost("register")]
        public IActionResult Register(RegisterFormDto form)
        {
            try
            {
                _service.Register(form);
                return NoContent();

            }
            catch (Exception)
            {
                return BadRequest();
            };
        }

        [HttpPost("login")]
        public IActionResult Login(LoginFormDto form)
        {
            PayloadDto user = _service.Login(form);
            if(user != null)
            {
                string token = _tokenService.Encode(user);
                return Ok(token);
            }
            return Unauthorized();
        }
    }
}
