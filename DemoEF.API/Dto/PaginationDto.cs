﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoEF.API.Dto
{
    public class PaginationDto<T>
    {
        public int Total { get; set; }
        public int Offset { get; set; }
        public int Limit { get; set; }
        public IEnumerable<T> Results { get; set; }
    }
}
