﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DemoEF.API.Dto.Forms
{
    public class RegisterFormDto
    {
        [Required]
        [MaxLength(255)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string PlainPassword { get; set; }

        [Required]
        public string Role { get; set; }
    }
}
