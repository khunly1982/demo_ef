﻿using EFDemo.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DemoEF.API.Dto.Forms
{
    public class CocktailFormDto
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Range(0, 99.99)]
        public decimal? Price { get; set; }

        public string Description { get; set; }

        public ImageTypes MimeType { get; set; }

        public byte[] File { get; set; }
    }
}
