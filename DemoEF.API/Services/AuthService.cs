﻿using DemoEF.API.Dto.Forms;
using EFDemo;
using EFDemo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.Security;
using System.Security.Cryptography;
using System.Text;
using DemoEF.API.Dto;

namespace DemoEF.API.Services
{
    public class AuthService
    {
        private readonly FiestaContext _context;

        public AuthService(FiestaContext context)
        {
            _context = context;
        }

        public void Register(RegisterFormDto dto)
        {
            Guid salt = Guid.NewGuid();
            _context.Add(new User
            {
                Email = dto.Email,
                Role = dto.Role,
                Salt = salt,
                Password = HashPassword(dto.PlainPassword, salt)
            });
            _context.SaveChanges();
        }

        private byte[] HashPassword(string plainPassword, Guid salt)
        {
            HashAlgorithm algo = new SHA512CryptoServiceProvider();
            return algo.ComputeHash(Encoding.UTF8.GetBytes(plainPassword + salt.ToString()));
        }

        public PayloadDto Login(LoginFormDto form)
        {
            User u = _context.Users.FirstOrDefault(u => u.Email == form.Email);
            if (u is null) return null;

            byte[] hash = HashPassword(form.PlainPassword, u.Salt);

            if(Encoding.UTF8.GetString(u.Password) == Encoding.UTF8.GetString(hash))
            {
                return new PayloadDto { 
                    Id = u.Id,
                    Email = u.Email,
                    Role = u.Role
                };
            }
            return null;
        }
    }
}
