﻿using DemoEF.API.Dto;
using DemoEF.API.Dto.Forms;
using EFDemo;
using EFDemo.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DemoEF.API.Services
{
    public class CategoryService
    {
        private readonly FiestaContext _context;

        public CategoryService(FiestaContext context)
        {
            _context = context;
        }

        public IEnumerable<CategoryDto> FindAll()
        {
            return _context.Categories.Select(c => new CategoryDto
            {
                Id = c.Id,
                Name = c.Name
            });
        }

        public CategoryDetailsDto FindById(int id)
        {
            Category c = _context.Categories
                // joindre les Ingredients
                .Include(c => c.Ingredients)
                .FirstOrDefault(c => c.Id == id);

            if (c is null)
                return null;

            // mapper c => CategoryDetailsDto
            return new CategoryDetailsDto
            {
                Id = c.Id,
                Name = c.Name,
                Ingredients = c.Ingredients.Select(i => new IngredientDto
                {
                    Id = i.Id,
                    Name = i.Name,
                })
            };
        }

        public void Add(CategoryFormDto form)
        {
            _context.Add(new Category { Name = form.Name });
            _context.SaveChanges();
        }

        public void Update(CategoryFormDto form)
        {
            Category c = _context.Categories.Find(form.Id);
            c.Name = form.Name;
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            Category c = _context.Categories.Find(id);
            _context.Remove(c);
            _context.SaveChanges();
        }
    }
}
