﻿using EFDemo;
using EFDemo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoEF.API.Services
{
    public class ImageService
    {
        private readonly FiestaContext _context;

        public ImageService(FiestaContext context)
        {
            _context = context;
        }

        public Image FindById(int id)
        {
            return _context.Images.Find(id);
        }
    }
}
