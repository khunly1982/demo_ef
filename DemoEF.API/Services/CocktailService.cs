﻿using DemoEF.API.Dto;
using DemoEF.API.Dto.Forms;
using EFDemo;
using EFDemo.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace DemoEF.API.Services
{
    public class CocktailService
    {

        private readonly FiestaContext _context;

        private readonly IngredientService _iService;

        public CocktailService(FiestaContext context, IngredientService service)
        {
            _context = context;
            _iService = service;
        }

        public void Add(CocktailFormDto form)
        {
            using(TransactionScope transaction = new TransactionScope())
            {
                EntityEntry<Cocktail> entry = _context.Cocktails.Add(new Cocktail
                {
                    Name = form.Name,
                    Price = form.Price,
                    Description = form.Description,
                });

                _context.SaveChanges();

                int id = entry.Entity.Id;
                _context.Images.Add(new Image
                {
                    CocktailId = id,
                    File = form.File,
                    MimeType = form.MimeType
                });

                _context.SaveChanges();

                // commit la transaction
                transaction.Complete();
            }
        }

        public PaginationDto<CocktailDto> Find(int limit, int offset)
        {
            PaginationDto<CocktailDto> dto = new PaginationDto<CocktailDto>
            {
                Total = _context.Cocktails.Count(),
                Limit = limit,
                Offset = offset,
                Results = _context.Cocktails
                    .Skip(offset)
                    .Take(limit)
                    .Include(c => c.Image)
                    .Select(c => new CocktailDto { 
                        Id = c.Id,
                        Name = c.Name,
                        ImageUrl = c.Image != null ? "/api/Image/" + c.Image.Id : null
                    })
            };
            return dto;
        }

        public CocktailDetailsDto FindById(int id)
        {
            Cocktail c = _context.Cocktails.Find(id);
            Image image = _context.Images.FirstOrDefault(i => i.CocktailId == c.Id);
            return new CocktailDetailsDto
            {
                Id = c.Id,
                Name = c.Name,
                Price = c.Price,
                Recipe = _iService.FindByCocktail(c.Id),
                ImageUrl = image != null ? "/api/Image/" + image.Id : null,
            };
        }
    }
}
