﻿using DemoEF.API.Dto;
using DemoEF.API.Dto.Forms;
using EFDemo;
using EFDemo.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoEF.API.Services
{
    public class IngredientService
    {
        private readonly FiestaContext _context;

        public IngredientService(FiestaContext context)
        {
            _context = context;
        }

        public IEnumerable<IngredientRecipeDto> FindByCocktail(int cocktailId)
        {
            return _context.Recipes.Where(r => r.CocktailId == cocktailId)
                .Include(r => r.Ingredient)
                .Select(r => new IngredientRecipeDto {
                    Id = r.Ingredient.Id,
                    Name = r.Ingredient.Name,
                    Quantity = r.Quantity
                });
        }

        public IngredientDto FindById(int id)
        {
            Ingredient i = _context.Ingredients.Find(id);
            if (i is null) return null;
            return new IngredientDto
            {
                Id = i.Id,
                Name = i.Name
            };
        }

        public void Add(IngredientFormDto form)
        {
            _context.Add(new Ingredient
            {
                Name = form.Name,
                CategoryId = form.CategoryId
            });
            _context.SaveChanges();
        }

        public void Update(IngredientFormDto form)
        {
            Ingredient i = _context.Ingredients.Find(form.Id);

            i.Name = form.Name;
            i.CategoryId = form.CategoryId;

            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            Ingredient i = _context.Ingredients.Find(id);
            _context.Remove(i);
            _context.SaveChanges();
        }
    }
}
