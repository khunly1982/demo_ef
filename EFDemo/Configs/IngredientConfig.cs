﻿using EFDemo.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo.Configs
{
    class IngredientConfig : IEntityTypeConfiguration<Ingredient>
    {
        public void Configure(EntityTypeBuilder<Ingredient> builder)
        {
            builder.Property(i => i.Name)
                .HasMaxLength(100)
                .IsRequired();

            builder.HasOne(i => i.Category)
                .WithMany(c => c.Ingredients)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
