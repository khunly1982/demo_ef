﻿using EFDemo.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo.Configs
{
    class CocktailConfig : IEntityTypeConfiguration<Cocktail>
    {
        public void Configure(EntityTypeBuilder<Cocktail> builder)
        {
            builder.Property(c => c.Name)
                .HasMaxLength(100)
                .IsRequired();

            builder.HasIndex(c => c.Name).IsUnique();

            builder.Property(c => c.Price)
                .HasColumnType("DECIMAL(4,2)");

            builder.HasOne(c => c.Image).WithOne(i => i.Cocktail)
                .HasForeignKey<Image>(i => i.CocktailId);
        }
    }
}
