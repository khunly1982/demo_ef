﻿using EFDemo.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo.Configs
{
    class CategoryConfig : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            // définit le nom de la table qui sera crée
            builder.ToTable("Categories");

            // définit la primary key par defaut AI
            builder.HasKey(c => c.Id);

            // pour ne pas avoir un id AI
            // builder.Property(c => c.Id).ValueGeneratedNever();

            builder.Property(c => c.Name)
                //définit un nom pour la colonne
                //.HasColumnName("name")
                .HasMaxLength(50)
                .IsUnicode(false)
                .IsRequired();

            builder.HasIndex(c => c.Name).IsUnique();
        }
    }
}
