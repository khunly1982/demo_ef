﻿using EFDemo.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo.Configs
{
    class ImageConfig : IEntityTypeConfiguration<Image>
    {
        public void Configure(EntityTypeBuilder<Image> builder)
        {
            builder.Property(i => i.MimeType)
                .HasConversion<string>().HasMaxLength(50)
                .IsRequired().IsUnicode(false);

            builder.Property(i => i.File)
                .IsRequired();
        }
    }
}
