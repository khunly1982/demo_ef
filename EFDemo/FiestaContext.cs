﻿using EFDemo.Configs;
using EFDemo.Entities;
using EFDemo.Views;
using Microsoft.EntityFrameworkCore;

namespace EFDemo
{
    public class FiestaContext : DbContext
    {
        private readonly string _defaultConnectionString = 
            @"data source=K-PC\SQLSERVER;initial catalog=fiesta_db;integrated security=true";

        public DbSet<Category> Categories { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<Cocktail> Cocktails { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<User> Users { get; set; }

        public DbSet<VIngredient> VIngredients { get; set; }

        /// <summary>
        /// Configuration de votre context
        /// </summary>
        /// <param name="builder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder builder) 
        {
            builder.UseSqlServer(_defaultConnectionString);
            // builder.UseLazyLoadingProxies();
        }

        /// <summary>
        /// Méthode appelée qui permettra de créer vos migrations 
        /// </summary>
        /// <param name="builder"></param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new CategoryConfig());
            builder.ApplyConfiguration(new IngredientConfig());
            builder.ApplyConfiguration(new RecipeConfig());
            builder.ApplyConfiguration(new CocktailConfig());
            builder.ApplyConfiguration(new ImageConfig());
            builder.ApplyConfiguration(new UserConfig());

            builder.Entity<VIngredient>().ToView("VIngredient");
        }
    }
}
