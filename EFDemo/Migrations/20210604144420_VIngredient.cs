﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EFDemo.Migrations
{
    public partial class VIngredient : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            string query = "CREATE VIEW VIngredient " +
                "AS SELECT i.Id, i.[Name], i.[CategoryId], c.[Name] AS CategoryName " +
                "FROM Ingredients i " +
                "JOIN Categories c ON i.CategoryId = c.Id";
            migrationBuilder.Sql(query);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
