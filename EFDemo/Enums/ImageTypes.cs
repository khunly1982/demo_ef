﻿using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace EFDemo.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ImageTypes
    {
        [EnumMember(Value = "image/jpg")]
        ImageJpg,
        [EnumMember(Value = "image/png")]
        ImagePng,
        [EnumMember(Value = "image/svg")]
        ImageSvg
    }
}
