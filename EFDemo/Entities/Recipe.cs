﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo.Entities
{
    public class Recipe
    {
        public int Id { get; set; }
        public int CocktailId { get; set; }
        public int IngredientId { get; set; }
        public string Quantity { get; set; }
        public virtual Cocktail Cocktail { get; set; }
        public virtual Ingredient Ingredient { get; set; }
    }
}
