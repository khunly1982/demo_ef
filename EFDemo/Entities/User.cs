﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo.Entities
{
    public class User
    {
        public int Id { get; set; }

        public string Role { get; set; }

        public byte[] Password { get; set; }

        public string Email { get; set; }

        public Guid Salt { get; set; }
    }
}
