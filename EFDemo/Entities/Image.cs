﻿using EFDemo.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo.Entities
{
    public class Image
    {
        public int Id { get; set; }
        public byte[] File { get; set; }
        public ImageTypes MimeType { get; set; }
        public int CocktailId { get; set; }

        public virtual Cocktail Cocktail { get; set; }
    }
}
