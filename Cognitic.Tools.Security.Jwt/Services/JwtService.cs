﻿using Cognitic.Tools.Security.Jwt.Configuration;
using Cognitic.Tools.Security.Jwt.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Cognitic.Tools.Security.Jwt.Services
{
    public class JwtService
    {
        private readonly JwtConfiguration _config;

        public JwtService(JwtConfiguration config)
        {
            _config = config;
        }

        public string Encode(IPayload payload)
        {
            JwtSecurityTokenHandler _handler = new JwtSecurityTokenHandler();
            JwtSecurityToken token = new JwtSecurityToken(
                // issuer
                _config.Issuer,
                // audience,
                _config.Audience,
                // payload
                ToClaims(payload),
                // nbf
                DateTime.UtcNow,
                // exp
                DateTime.UtcNow.AddSeconds(_config.Exp),
                // signature,
                new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.Signature)),
                    SecurityAlgorithms.HmacSha256
                )
            );
            return _handler.WriteToken(token);
        }

        public ClaimsPrincipal Decode(string token)
        {
            // valider le token
            JwtSecurityTokenHandler _handler = new JwtSecurityTokenHandler();
            try
            {
                return _handler.ValidateToken(token, new TokenValidationParameters { 
                    ValidateIssuer = _config.ValidateIssuer,
                    ValidIssuer = _config.Issuer,
                    ValidateAudience = _config.ValidateAudience,
                    ValidAudience = _config.Audience,
                    RequireExpirationTime = _config.ValidateExp,
                    ValidateLifetime = _config.ValidateExp,
                    RequireSignedTokens = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.Signature)),
                }, out SecurityToken sToken);
            }
            catch(Exception e)
            {
                Debug.WriteLine(e.Message);
                return null;
            }
        }

        private IEnumerable<Claim> ToClaims(IPayload payload)
        {
            yield return new Claim(ClaimTypes.Email, payload.Email);
            yield return new Claim(ClaimTypes.PrimarySid, payload.Id.ToString());
            yield return new Claim(ClaimTypes.Role, payload.Role);

            foreach (PropertyInfo p in payload.GetType().GetProperties()
                .Where(prop => prop.CanRead && prop.GetValue(payload) != null))
            {
                yield return new Claim(p.Name.ToLower(), p.GetValue(payload).ToString());
            }
        }
    }
}
