﻿using EFDemo;
using EFDemo.Entities;
using EFDemo.Views;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EFTest
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] un = new byte[1] { 5 };
            byte[] deux = new byte[1] { 5 };

            Console.WriteLine( Encoding.UTF8.GetString(un) == Encoding.UTF8.GetString(deux));

            // using (FiestaContext dc = new FiestaContext())
            {
                //// ajouter une categorie dans la db
                //dc.Categories.Add(new Category 
                //{ 
                //    Name = "Fruit"
                //});

                //// ajouter un ingrédient
                //dc.Ingredients.Add(new Ingredient
                //{
                //    Name = "Rhum",
                //    CategoryId = 1
                //});

                //dc.Ingredients.Add(new Ingredient
                //{
                //    Name = "Fraise",
                //    CategoryId = 2
                //});

                //// apres avoir modifier des tables toujours sauvegarder
                //dc.SaveChanges();

                // SELECT Linq Rajoute des clauses à la selection
                //IEnumerable<Ingredient> igs
                //    = dc.Ingredients
                //        .Where(i => i.CategoryId == 1);
                        //.Include(i => i.Category);
                //    //.Skip(40).Take(10);
                //foreach (var i in igs)
                //{
                //    Console.WriteLine(i.Name);
                //    Console.WriteLine(i.Category.Name);
                //}

                // SELECT * FROM Category WHERE Id = 2
                //Category c = dc.Categories.Find(2);
                // Update
                //c.Name = "Legumes";
                //dc.SaveChanges();

                // DELETE
                //Category c = dc.Categories.Find(2);
                //dc.Remove(c);
                //dc.SaveChanges();

                //foreach (VIngredient i in dc.VIngredients)
                //{
                //    Console.WriteLine(i.Name);
                //    Console.WriteLine(i.CategoryName);
                //}
            }
        }
    }
}
